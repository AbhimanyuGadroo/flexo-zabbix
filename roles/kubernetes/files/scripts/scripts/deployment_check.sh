#!/bin/bash
status=`sudo kubectl get deployment $1 | awk '{print $4}' | sed '1d'`
if [ "$status" -ge 1 ] 
	then
		echo "1"
else
	echo "0"
fi
exit
