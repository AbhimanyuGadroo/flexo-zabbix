#!/bin/sh
	echo '{'
	echo ' "data":['
	ser=`sudo kubectl get deployment | awk '{print $1}' | sed '1d'`
	for i in $ser
	do 
		echo '{ "{#DEPLOYMENT}":"'${i}'" },'
	done
	echo '{ "{DEPLOYMENT}":"" }'
	echo ' ]'
	echo '}'
